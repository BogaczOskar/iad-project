package shapes;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;

public class Ellipse implements PointsGenerable, Cloneable {

  public Ellipse(Point2D.Double center, double firstRadius, double secondRadius) {
    this.center = center;
    this.firstRadius = firstRadius;
    this.secondRadius = secondRadius;
  }

  @Override
  public Point2D.Double generatePoint() {
    Random rand = new Random();
    
    double x, y;
    double angle = rand.nextDouble() * 2 * Math.PI;
    double randFirstRadius = rand.nextDouble() * this.firstRadius;
    double randSecondRadius = rand.nextDouble() * this.secondRadius;
    
    x = this.center.getX() + randFirstRadius * Math.cos(angle);
    y = this.center.getY() + randSecondRadius * Math.sin(angle);
    
    System.out.println("[" + x + "," + y + "]");
    
    return new Point2D.Double(x, y);
  }

  @Override
  public ArrayList<Point2D.Double> generatePoints(int amount) {
    ArrayList<Point2D.Double> points = new ArrayList<>();
    
    for (int i = 0; i < amount; i++) {
      points.add(this.generatePoint());
    }
    
    return points;
  }
  
  @Override
  public Ellipse clone() throws CloneNotSupportedException {
    Ellipse cloned = (Ellipse) super.clone();
    cloned.setCenter(this.getCenter());
    return cloned;
  }
  
  public Point2D.Double getCenter() {
    return (Point2D.Double) center.clone();
  }

  public void setCenter(Point2D.Double center) {
    this.center = center;
  }

  public double getFirstRadius() {
    return firstRadius;
  }

  public void setFirstRadius(double firstRadius) {
    this.firstRadius = firstRadius;
  }

  public double getSecondRadius() {
    return secondRadius;
  }

  public void setSecondRadius(double secondRadius) {
    this.secondRadius = secondRadius;
  }

  private Point2D.Double center;
  private double firstRadius;
  private double secondRadius;
  
}
