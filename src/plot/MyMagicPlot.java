package plot;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class MyMagicPlot {
	
	public static final int DEFAULT_CHART_WIDTH = 600; 
	public static final int DEFAULT_CHART_HEIGHT = 400;
	
	public MyMagicPlot() {
		this.graphWidth = DEFAULT_CHART_WIDTH;
		this.graphHeight = DEFAULT_CHART_HEIGHT;
	}
	
	public MyMagicPlot(int width, int height) {
		this.graphWidth = width;
		this.graphHeight = height;
	}
	
	/**
	 * Tworzy wykres liniowy z punktami polaczonymi prostymi
	 * @param points zbior punktow
	 * @param graphName tytul wykresu
	 * @return zwraca obiekt utworzonego wykresu
	 */
	public JFreeChart createXYLineGraph( ArrayList<Point2D.Double> points, String graphName ) {
		
		XYSeries series = this.createXYSeries( points, graphName );
		
		XYSeriesCollection dataSet = new XYSeriesCollection( series );
		
		JFreeChart chart = ChartFactory.createXYLineChart(
				graphName, 
				"x-axis", 
				"y-axis", 
				dataSet, 
				PlotOrientation.VERTICAL,
				true, 
				true, 
				false);
		
		return chart;
	}
	
	/**
	 * Tworzy wykres
	 * @param graphName nazwa wykresu
	 * @param dataset zbior danych
	 * @param renderer sposob rysowania w postaci renderera
	 * @return
	 */
	public JFreeChart createGraph(String graphName, XYDataset dataset, XYItemRenderer renderer) {
    JFreeChart chart = ChartFactory.createXYLineChart(graphName, "x-axis", "y-axis", dataset);

    XYPlot plot = chart.getXYPlot();
    plot.setRenderer(renderer);
    plot.setBackgroundPaint(new Color(240, 240, 240));
    plot.setDomainGridlinePaint(Color.gray);
    plot.setRangeGridlinePaint(Color.gray);
    
    NumberAxis axis = (NumberAxis) plot.getRangeAxis();
    axis.setAutoRangeIncludesZero(false);
    
    return chart;
  }
	
	/**
	 * Tworzy wykres z punktami
	 * @param points zbior punktow
	 * @param graphName tytul wykresu
	 * @return zwraca obiekt utworzonego wykresu
	 */
	public JFreeChart createXYScatterGraph( ArrayList<Point2D.Double> points, String graphName ) {
		
		XYSeries series = this.createXYSeries( points, graphName );
		
		XYSeriesCollection dataSet = new XYSeriesCollection( series );
		
		JFreeChart chart = ChartFactory.createScatterPlot(graphName, "x-axis", "y-axis", dataSet);
		
		return chart;
	}
	
	/**
	 * Zapisuje przeslany wykres do pliku .jpg
	 * @param chart wykres
	 * @param fileName nazwa pliku
	 */
	public void saveGraphToJPG( JFreeChart chart, String fileName ) {
		try {
			ChartUtilities.saveChartAsJPEG(
					new File( fileName ), 
					chart, 
					this.graphWidth, 
					this.graphHeight);
		} catch ( IOException e ) {
			System.out.println( "Blad zapisu grafu do pliku" );
			e.printStackTrace( );
		}
	}

	public XYSeries createXYSeries( ArrayList<Point2D.Double> points, String graphName ) {
    XYSeries series = new XYSeries( graphName );
    for (Point2D.Double point : points) {
      series.add(point.getX(), point.getY());
    }
    
    return series;
  }
	
	public XYDataset createDataset(ArrayList<Point2D.Double> points, String title) {
    XYSeries series = new XYSeries(title);
    for (Point2D.Double point : points) {
      series.add(point.x, point.y);
    }
    return new XYSeriesCollection(series);
  }
  
  public JFreeChart addDataset(JFreeChart chart, XYDataset dataset, XYItemRenderer renderer) {
    XYPlot plot = chart.getXYPlot();
    int datasetCount = plot.getDatasetCount();
    plot.setDataset(datasetCount, dataset);
    plot.setRenderer(datasetCount, renderer);
    return chart;
  }
	
	private int graphWidth;
  private int graphHeight;
	
}

// XYSplineRenderer() - gladka linia
// XYLineAndShapeRenderer(false, true) - same punkty
