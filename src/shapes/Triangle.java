package shapes;

import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.util.ArrayList;
import java.util.Random;

public class Triangle implements PointsGenerable, Cloneable {
  
  public Triangle(Point2D.Double a, Point2D.Double b, Point2D.Double c) {
    this.a = a;
    this.b = b;
    this.c = c;
  }

  @Override
  public Double generatePoint() {
    
    Random rand = new Random();
    double x, y;
    double r1, r2;
    
    r1 = rand.nextDouble();
    r2 = rand.nextDouble();
    
    x = (1 - Math.sqrt(r1)) * this.a.getX() +
        (Math.sqrt(r1) * (1 - r2)) * this.b.getX() +
        (Math.sqrt(r1) * r2) * this.c.getX();
    
    y = (1 - Math.sqrt(r1)) * this.a.getY() +
        (Math.sqrt(r1) * (1 - r2)) * this.b.getY() +
        (Math.sqrt(r1) * r2) * this.c.getY();
    
    return new Point2D.Double(x, y);
  }

  @Override
  public ArrayList<Point2D.Double> generatePoints(int amount) {
    ArrayList<Point2D.Double> points = new ArrayList<>();
    
    for (int i = 0; i < amount; i++) {
      points.add(this.generatePoint());
    }
    
    return points;
  }
  
  @Override
  public Triangle clone() throws CloneNotSupportedException {
    Triangle cloned = (Triangle) super.clone();
    cloned.setA(this.getA());
    cloned.setB(this.getB());
    cloned.setC(this.getC());
    
    return cloned;
  }
  
  public Point2D.Double getA() {
    return (Point2D.Double) a.clone();
  }

  public void setA(Point2D.Double a) {
    this.a = a;
  }

  public Point2D.Double getB() {
    return (Point2D.Double) b.clone();
  }

  public void setB(Point2D.Double b) {
    this.b = b;
  }

  public Point2D.Double getC() {
    return (Point2D.Double) c.clone();
  }

  public void setC(Point2D.Double c) {
    this.c = c;
  }

  private Point2D.Double a;
  private Point2D.Double b;
  private Point2D.Double c;

}
