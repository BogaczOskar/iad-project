package configuration;

import com.thoughtworks.xstream.XStreamException;

import utils.XStreamUtils;

public class Configuration {

	private static final String CONFIG_FILE_PATH = "configuration.xml";
	
	private static Configuration instance = null;
	
	private int inputsNeuronsNumber = 4;
	private int hiddenNeuronsNumber = 2;
	private int outputsNeuronsNumber = 4;
	
	private boolean isBias = true;
	private double momentum = 0.9;
	
	private double learningSpeedFactor = 0.6;
	private boolean isCheckError = true;
	private double expectedError = 0.001;
	
	private String errorFilePath = "error.jpg";
	private String reportFilePath = "report.txt";
	private String graphDescription = "";
	private String reportComment = "";
	
	public static Configuration getConfiguration() {
	    if ( null == instance ) {
	    	try {
	    		instance = (Configuration) XStreamUtils.fromXMLFile( CONFIG_FILE_PATH );
	    	} catch ( XStreamException e ) {
	    		System.out.println(e.getMessage());
	    	}
	    }
	    
	    return instance;
	}
	
	public int getInputsNeuronsNumber() {
		return inputsNeuronsNumber;
	}
	public int getHiddenNeuronsNumber() {
		return hiddenNeuronsNumber;
	}
	public int getOutputsNeuronsNumber() {
		return outputsNeuronsNumber;
	}
	public boolean isBias() {
		return isBias;
	}
	public double getMomentum() {
		return momentum;
	}
	public double getLearningSpeedFactor() {
		return learningSpeedFactor;
	}
	public boolean isCheckError() {
		return isCheckError;
	}
	public double getExpectedError() {
		return expectedError;
	}
	public String getErrorFilePath() {
		return errorFilePath;
	}
	public String getReportFilePath() {
		return reportFilePath;
	}
	public String getGraphDescription() {
		return graphDescription;
	}
	public String getReportComment() {
		return reportComment;
	}
	
}
