package neuralNetwork;

import activationfunctions.Calculable;

public class HiddenLayer {
	
	private SimpleNeuralCell[] neurons;
	private double[] inputs;
	private double[] outputs;
	private double[] deltaWeights;
	
	
	public HiddenLayer(int neuronsNumber, int inputsNumber, Calculable activFunc, boolean bias, double momentum) {
		this.neurons = new SimpleNeuralCell[neuronsNumber];
		for (int i = 0; i < this.neurons.length; i++) {
			this.neurons[i] = new SimpleNeuralCell(activFunc, inputsNumber, bias, momentum);
		}
		
		this.outputs = new double[neuronsNumber];
		this.deltaWeights = new double[neuronsNumber];
	}
	
	public void compute(double... inputs) {
		this.inputs = inputs;
		for (int i = 0; i < this.outputs.length; i++) {
			this.outputs[i] = this.neurons[i].calcOutput(inputs);
		}
	}
	
	public void learn(double speedFactor){
        for (int i = 0; i < this.neurons.length; i++){
        	this.neurons[i].learn(speedFactor, this.deltaWeights[i], inputs);
        }
    }

	public void computeBackPropagation(SimpleNeuralCell[] nextLayer, double[] deltaWeights) {
		double tmp = 0.0;
		for (int i = 0; i < this.neurons.length; i++) {
			for (int j = 0; j < nextLayer.length; j++) {
				tmp += deltaWeights[j] * nextLayer[j].getWeight(i);
			}
			
			double d = this.neurons[i].getValueOfDerivativeActivationFunc(this.neurons[i].calcSum(this.inputs));
			this.deltaWeights[i] = tmp * d;
			tmp = 0.0;
		}
	}
	
	public SimpleNeuralCell[] getNeurons() {
		return neurons.clone();
	}

	public double[] getInputs() {
		return inputs.clone();
	}

	public double[] getOutputs() {
		return outputs.clone();
	}

	public double[] getDeltaWeights() {
		return deltaWeights.clone();
	}
	
	
}
