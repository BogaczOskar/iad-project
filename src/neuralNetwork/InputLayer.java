package neuralNetwork;

import activationfunctions.Calculable;

public class InputLayer {
	private SimpleNeuralCell[] neurons;
	private double[] outputs;
	
	public InputLayer(int neuronsNumber, Calculable activationFunc) {
		this.neurons = new SimpleNeuralCell[neuronsNumber];
		for (int i = 0; i < this.neurons.length; i++) {
			this.neurons[i] = new SimpleNeuralCell(activationFunc, false);
		}
		
		this.outputs = new double[neuronsNumber];
	}
	
	public void compute(double... inputs) {
		for (int i = 0; i < this.neurons.length; i++) {
			outputs[i] = this.neurons[i].calcOutput(inputs[i]);
		}
	}

	public SimpleNeuralCell[] getNeurons() {
		return neurons.clone();
	}

	public double[] getOutputs() {
		return outputs.clone();
	}
	
}
