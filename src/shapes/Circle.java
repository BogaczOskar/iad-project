package shapes;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;

public class Circle implements PointsGenerable, Cloneable {
  
  public Circle(Point2D.Double center, double radius) {
    this.center = center;
    this.radius = radius;
  }
  
  @Override
  public Point2D.Double generatePoint() {
    Random rand = new Random();
    
    double x, y;
    double angle = rand.nextDouble() * 2 * Math.PI;
    double randRadius = rand.nextDouble() * this.radius;
    
    x = this.center.getX() + randRadius * Math.sin(angle);
    y = this.center.getY() + randRadius * Math.cos(angle);
        
    return new Point2D.Double(x, y);
  }

  @Override
  public ArrayList<Point2D.Double> generatePoints(int amount) {
    ArrayList<Point2D.Double> points = new ArrayList<>();
    
    for (int i = 0; i < amount; i++) {
      points.add(this.generatePoint());
    }
    
    return points;
  }
  
  @Override
  public Circle clone() throws CloneNotSupportedException {
    Circle cloned = (Circle) super.clone();
    cloned.setCenter(this.getCenter());
    return cloned;
  }
  
  public Point2D.Double getCenter() {
    return (Point2D.Double) center.clone();
  }
  public void setCenter(Point2D.Double center) {
    this.center = center;
  }
  public double getRadius() {
    return radius;
  }
  public void setRadius(double radius) {
    this.radius = radius;
  }
  
  private Point2D.Double center;
  private double radius;
}
