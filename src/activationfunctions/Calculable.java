package activationfunctions;

public interface Calculable {

  public double getValueOfFunc(double x);
  
  public double getValueOfDerivative(double x);
  
}
