package activationfunctions;

public class IdentityFunc implements Calculable{

	@Override
	public double getValueOfFunc(double x) {
		return x;
	}

	@Override
	public double getValueOfDerivative(double x) {
		return 0;
	}

}
