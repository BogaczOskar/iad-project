package shapes;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;

public class Quadrangle implements PointsGenerable, Cloneable{
  
  public Quadrangle(Point2D.Double a, Point2D.Double b, Point2D.Double c, Point2D.Double d) {
    this.a = a;
    this.b = b;
    this.c = c;
    this.d = d;
    this.firstTriangle = new Triangle(a, b, d);
    this.secondTriangle = new Triangle(b, c, d);
  }
  
  @Override
  public Point2D.Double generatePoint() {
    Point2D.Double point = null;
    
    if (new Random().nextBoolean()) {
      point = this.firstTriangle.generatePoint();
    } else {
      point = this.secondTriangle.generatePoint();
    }
    
    return point;
  }
  
  @Override
  public ArrayList<Point2D.Double> generatePoints(int amount) {
    ArrayList<Point2D.Double> points = new ArrayList<>();
    
    for (int i = 0; i < amount; i++) {
      points.add(this.generatePoint());
    }
    
    return points;
  }
  
  @Override
  public Quadrangle clone() throws CloneNotSupportedException {
    Quadrangle cloned = (Quadrangle) super.clone();
    cloned.setA(this.getA());
    cloned.setB(this.getB());
    cloned.setC(this.getC());
    cloned.setD(this.getD());
    cloned.setFirstTriangle(this.getFirstTriangle());
    cloned.setSecondTriangle(this.getSecondTriangle());
    
    return cloned;
  }
  
  public Point2D.Double getA() {
    return (Point2D.Double) a.clone();
  }

  public void setA(Point2D.Double a) {
    this.a = a;
  }

  public Point2D.Double getB() {
    return (Point2D.Double) b.clone();
  }

  public void setB(Point2D.Double b) {
    this.b = b;
  }

  public Point2D.Double getC() {
    return (Point2D.Double) c.clone();
  }

  public void setC(Point2D.Double c) {
    this.c = c;
  }

  public Point2D.Double getD() {
    return (Point2D.Double) d.clone();
  }

  public void setD(Point2D.Double d) {
    this.d = d;
  }

  public Triangle getFirstTriangle() {
    try {
      return (Triangle) firstTriangle.clone();
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
      return null;
    }
  }

  public void setFirstTriangle(Triangle firstTriangle) {
    this.firstTriangle = firstTriangle;
  }

  public Triangle getSecondTriangle() {
    try {
      return (Triangle) secondTriangle.clone();
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
      return null;
    }
  }

  public void setSecondTriangle(Triangle secondTriangle) {
    this.secondTriangle = secondTriangle;
  }

  private Point2D.Double a;
  private Point2D.Double b;
  private Point2D.Double c;
  private Point2D.Double d;
  private Triangle firstTriangle;
  private Triangle secondTriangle;

}
