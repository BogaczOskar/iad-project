package shapes;

import java.awt.geom.Point2D;
import java.util.ArrayList;

public interface PointsGenerable {
  
  /**
   * Generuje jeden losowy punkt
   * @return punkt
   */
  public Point2D.Double generatePoint();
  
  /**
   * Generuje kolekcje punktow
   * @param amount ilosc punktow
   * @return Kolekcja punktow
   */
  public ArrayList<Point2D.Double> generatePoints(int amount);
  
}
