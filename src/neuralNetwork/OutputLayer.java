package neuralNetwork;

import activationfunctions.Calculable;

public class OutputLayer {
	private SimpleNeuralCell[] neurons;
	private double[] inputs;
	private double[] outputs;
	private double[] deltaWeights;
	
	public OutputLayer(int neuronsNumber, int inputsNumber, Calculable activFunc, boolean bias, double momentum) {
		this.neurons = new SimpleNeuralCell[neuronsNumber];
		for (int i = 0; i < this.neurons.length; i++) {
			this.neurons[i] = new SimpleNeuralCell(activFunc, inputsNumber, bias, momentum);
		}
		
		this.outputs = new double[neuronsNumber];
		this.deltaWeights = new double[neuronsNumber];
	}
	
	public void compute(double... inputs) {
		this.inputs = inputs;
		for (int i = 0; i < this.outputs.length; i++) {
			this.outputs[i] = this.neurons[i].calcOutput(inputs);
		}
	}
	
	public void learn(double speedFactor) {
		for (int i = 0; i < this.neurons.length; i++) {
			neurons[i].learn(speedFactor, this.deltaWeights[i], inputs);
		}
	}
	
	public void computeBackPropagation(double... expectedValues) {
		for (int i = 0; i < this.neurons.length; i++) {
			double s = this.neurons[i].calcSum(this.inputs);
			double a = (expectedValues[i] - this.outputs[i]) * this.neurons[i].getValueOfDerivativeActivationFunc(s);
			this.deltaWeights[i] = a;
		}
	}

	public SimpleNeuralCell[] getNeurons() {
		return neurons.clone();
	}

	public double[] getInputs() {
		return inputs.clone();
	}

	public double[] getOutputs() {
		return outputs.clone();
	}

	public double[] getDeltaWeights() {
		return deltaWeights.clone();
	}

	
	
}
