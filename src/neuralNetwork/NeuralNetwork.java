package neuralNetwork;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;

import activationfunctions.IdentityFunc;
import activationfunctions.SigmoidFunc;

public class NeuralNetwork {
	private InputLayer inputLayer;
	private HiddenLayer[] hiddenLayers;
	private OutputLayer outputLayer;
	
	// Do raportu
	private double[] inputs;
	private boolean bias;
	private double momentum;
	
	public NeuralNetwork(int inputNeuronsNumber, int firstHiddenNeuronsNumber, int outputNeuronsNumber, boolean bias, double momentum) {
		this.bias = bias;
		this.momentum = momentum;
		
		this.inputLayer = new InputLayer(inputNeuronsNumber, new IdentityFunc());
		
		this.hiddenLayers = new HiddenLayer[1];
		this.hiddenLayers[0] = new HiddenLayer(firstHiddenNeuronsNumber, this.inputLayer.getNeurons().length, new SigmoidFunc(1), bias, momentum);
		
		this.outputLayer = new OutputLayer(outputNeuronsNumber, this.hiddenLayers[0].getNeurons().length, new SigmoidFunc(1), bias, momentum);
	}
	
	public void compute(double... inputs) {
		this.inputs = inputs; // do raportu
		this.inputLayer.compute(inputs);
		
		this.hiddenLayers[0].compute(this.inputLayer.getOutputs());
		
		for (int i = 1; i < this.hiddenLayers.length; i++) {
			double[] hiddenLayerOutputs = hiddenLayers[i - 1].getOutputs();
			this.hiddenLayers[i].compute(hiddenLayerOutputs);
		}
		
		this.outputLayer.compute(this.hiddenLayers[this.hiddenLayers.length - 1].getOutputs());
	}

	public void learn(double speedFactor) {
		this.outputLayer.learn(speedFactor);
		for (int i = 0; i < this.hiddenLayers.length; i++) {
			this.hiddenLayers[i].learn(speedFactor);
		}
	}
	
	public void computeBackPropagation(double... expectedValues) {
		this.outputLayer.computeBackPropagation(expectedValues);
		this.hiddenLayers[this.hiddenLayers.length - 1].computeBackPropagation(this.outputLayer.getNeurons(), this.outputLayer.getDeltaWeights());
		
		for (int i = this.hiddenLayers.length - 2; i > -1; i++) {
			this.hiddenLayers[i].computeBackPropagation(this.hiddenLayers[i + 1].getNeurons(), this.hiddenLayers[i + 1].getDeltaWeights());
		}
	}
	
	public double clacError(double... expected) {
		double[] outputs = this.getOutputs();
		double error = 0.0;
		for (int i = 0; i < outputs.length; i++) {
			error += Math.pow(expected[i] - outputs[i], 2);
		}
		
		return 0.5 * error;
	}
	


	public InputLayer getInputLayer() {
		return inputLayer;
	}

	public HiddenLayer[] getHiddenLayers() {
		return hiddenLayers;
	}

	public OutputLayer getOutputLayer() {
		return outputLayer;
	}
	
	public double[] getOutputs() {
		return this.outputLayer.getOutputs().clone();
	}
	
	public void generateRaport(String fileName, double[] expected, String comments){

		StringBuilder sb = new StringBuilder();
		
		sb.append("=== RAPORT KONCOWY ===\n\n");
		
		sb.append("Budowa sieci neuronowej:\n");
		sb.append("\tIlość neuronów warstwy wejściowej:\t" + this.inputLayer.getNeurons().length + "\n");
		sb.append("\tIlość neuronów warstwy ukrytej:   \t" + this.hiddenLayers[0].getNeurons().length + "\n");
		sb.append("\tIlość neuronów warstwy wyjściowej:\t" + this.outputLayer.getNeurons().length + "\n");
		sb.append("\tCzy bias:    \t" + this.bias + "\n");
		
		if (this.momentum != 0) {
			sb.append("\tCzy momentum:\t" + this.momentum + "\n\n");
		} else {
			sb.append("\tCzy momentum:\t" + false + "\n\n");
		}
		
		
		sb.append("Dane wejściowe: \t" + Arrays.toString(this.inputs) + "\n");
		sb.append("Dane oczekiwane:\t" + Arrays.toString(expected) + "\n");
		sb.append("Dane wyjściowe: \t" + Arrays.toString(this.getOutputs()) + "\n");
		sb.append("Błąd globalny:  \t" + this.clacError(expected) + "\n");
		
		sb.append("\nWartości i błędy na poszczególnych wyjściach:\n");
		double[] outputs = this.getOutputs();
		for (int i = 0; i < outputs.length; i++) {
			double error = outputs[i] - expected[i];
			sb.append("\tWartość na wyjściu " + i + " : " + outputs[i] + "\tBłąd: " + error + "\n");
		}
		
		sb.append("\nWagi neuronów warstwy wyjściowej\n");
		SimpleNeuralCell[] neurons = this.outputLayer.getNeurons();
		for (int i = 0; i < neurons.length; i++) {
			double[] w = neurons[i].getWeights();
			sb.append("\tNeuron nr [" + i + "] ma wagi: " + Arrays.toString(w) + "\n");
		}
		
		sb.append("\nWartości wyjściowe warstwy ukrytej: " + Arrays.toString(this.hiddenLayers[0].getOutputs()) + "\n");
		
		sb.append("\nWagi neuronów warstwy ukrytej\n");
		SimpleNeuralCell[] neurons2 = this.hiddenLayers[0].getNeurons();
		for (int i = 0; i < neurons2.length; i++) {
			double[] w = neurons2[i].getWeights();
			sb.append("\tNeuron nr [" + i + "] ma wagi: " + Arrays.toString(w) + "\n");
		}

		sb.append("\nSwoje notatki:\n");
		sb.append("\t" + comments + "\n");
		
		String res = sb.toString();
		
        try {
            PrintWriter writer = new PrintWriter(fileName);
            writer.write(res);
            writer.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
	}
    
}
