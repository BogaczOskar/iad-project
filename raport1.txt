=== RAPORT KONCOWY ===

Budowa sieci neuronowej:
	Ilość neuronów warstwy wejściowej:	4
	Ilość neuronów warstwy ukrytej:   	2
	Ilość neuronów warstwy wyjściowej:	4
	Czy bias:    	true
	Czy momentum:	0.5

Dane wejściowe: 	[1.0, 0.0, 0.0, 0.0]
Dane oczekiwane:	[1.0, 0.0, 0.0, 0.0]
Dane wyjściowe: 	[0.9510070828764589, 2.4920726836027117E-4, 0.039961813583987836, 0.038236188584689426]
Błąd globalny:  	0.002729660347471052

Wartości i błędy na poszczególnych wyjściach:
	Wartość na wyjściu 0 : 0.9510070828764589	Błąd: -0.048992917123541146
	Wartość na wyjściu 1 : 2.4920726836027117E-4	Błąd: 2.4920726836027117E-4
	Wartość na wyjściu 2 : 0.039961813583987836	Błąd: 0.039961813583987836
	Wartość na wyjściu 3 : 0.038236188584689426	Błąd: 0.038236188584689426

Wagi neuronów warstwy wyjściowej
	Neuron nr [0] ma wagi: [-6.834392175747185, -7.046952588305136]
	Neuron nr [1] ma wagi: [5.647633844307306, 5.839486631976853]
	Neuron nr [2] ma wagi: [-7.34600298600429, 6.7934831796777715]
	Neuron nr [3] ma wagi: [6.74043100292914, -7.008748207657093]

Wartości wyjściowe warstwy ukrytej: [0.01980321137326442, 0.013463724076586515]

Wagi neuronów warstwy ukrytej
	Neuron nr [0] ma wagi: [-3.484609979045414, 4.3976168499855905, -4.19562387429335, 3.757897511568564]
	Neuron nr [1] ma wagi: [-3.5359988377318863, 5.017381112940684, 3.494142015637927, -3.7203763846988798]
