package activationfunctions;

public class SigmoidFunc implements Calculable{
  private double betaFactor;

  public SigmoidFunc(double betaFactor) {
    this.betaFactor = betaFactor;
  }

  public double getValueOfFunc(double x) {
    return 1 / (1 + Math.exp(-this.betaFactor * x));
  }
  
  public double getValueOfDerivative(double x) {
    return (this.betaFactor * Math.exp(this.betaFactor * x)) / 
        Math.pow(Math.exp(this.betaFactor * x) + 1, 2);
  }
  
  public double getBetaFactor() {
    return betaFactor;
  }

  public void setBetaFactor(double betaFactor) {
    this.betaFactor = betaFactor;
  }
}
