package main;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;

import configuration.Configuration;
import neuralNetwork.NeuralNetwork;
import plot.MyMagicPlot;

public class Main {
	
	public static void main(String[] args) {
    
	  	MyMagicPlot plot = new MyMagicPlot();
	  	Configuration conf = Configuration.getConfiguration();
	  	
	  	ArrayList<Point2D.Double> errors = new ArrayList<>();
	  	
		double [][] var = {
			{1, 0, 0, 0},
			{0, 1, 0, 0},
			{0, 0, 1, 0},
			{0, 0, 0, 1}
		};
	
    	NeuralNetwork nn = new NeuralNetwork(conf.getInputsNeuronsNumber(), 
    			conf.getHiddenNeuronsNumber(), 
    			conf.getOutputsNeuronsNumber(), 
    			conf.isBias(), 
    			conf.getMomentum());
    
    	//---------------------------------------------------------------------------------
    	
    	for (int i = 0; i < 100000; i++) {
			double error = 0.0;
			
			for (int j = 0; j < var.length; j++) {
				double[] inputs = var[j];
				double[] expected = var[j];
				
				nn.compute(inputs);
				
				error += nn.clacError(expected);
				
				nn.computeBackPropagation(expected);
				
				nn.learn(conf.getLearningSpeedFactor());
			}
			
			errors.add(new Point2D.Double(i, error));
			
			if (conf.isCheckError() && error < conf.getExpectedError()) {
				System.out.println("Iteracja, w której osiągnięto zadany błąd: " + i);
				break;
			}
		}
    	
    	//---------------------------------------------------------------------------------
    	
    	// Sprawdzenie jak się nauczyło
	    nn.compute(var[0]);
	    System.out.println("my: "+
	    Arrays.toString(nn.getOutputs()));
	    
	    // Zapis błędu
	    XYDataset data = plot.createDataset(errors, "error");
	    JFreeChart chart = plot.createGraph(conf.getGraphDescription(), data, new XYLineAndShapeRenderer(true, false));
	    plot.saveGraphToJPG(chart, conf.getErrorFilePath());
    
	    // Zapis raportu
    	nn.generateRaport(conf.getReportFilePath(), var[0], conf.getReportComment());
    	
    	System.out.println("done");
      
  	}

}
