package neuralNetwork;

import java.util.concurrent.ThreadLocalRandom;

import activationfunctions.Calculable;

public class SimpleNeuralCell {

  private double[] weights;
  private Calculable activationFunc;
  private boolean bias;
  private double momentum;
  private double biasWeight;
  
  private double[] memoredDelta;
 
  public SimpleNeuralCell(Calculable func, boolean bias, double... weights) {
	  this.weights = weights;
	  this.activationFunc = func;
	  this.bias = bias;
	  if(bias)
		  biasWeight = ThreadLocalRandom.current().nextDouble(-1.0, 1.0);//new Random().nextDouble();
	  this.memoredDelta = new double[weights.length];
  }
  
  public SimpleNeuralCell(Calculable func, boolean bias) {
	  this.activationFunc = func;
	  this.bias = bias;
	  this.weights = new double[] {1.0};
	  if(bias)
		  biasWeight = ThreadLocalRandom.current().nextDouble(-1.0, 1.0);
  }
  
  public SimpleNeuralCell(Calculable func, int inputsNumber, boolean bias, double momentum) {
	  this.weights = new double[inputsNumber];
	  for (int i = 0; i < weights.length; i++) {
		  this.weights[i] = ThreadLocalRandom.current().nextDouble(-1.0, 1.0);
	  }
	  
	  this.activationFunc = func;
	  this.bias = bias;
	  if(bias) {
		  biasWeight = ThreadLocalRandom.current().nextDouble(-1.0, 1.0);
	  }
	  
	  this.momentum = momentum;
	  this.memoredDelta = new double[inputsNumber];
  }
  
  public void learn(double speedFactor, double deltaWeight, double...inputs) {
	  double tmp = 0;
	  for (int i = 0; i < this.weights.length; i++) {
		  
		  double delta = speedFactor * (deltaWeight * inputs[i]);
		  
		  tmp = this.weights[i] + delta + this.momentum * this.memoredDelta[i];
		  
		  this.weights[i] = tmp;
		  
		  this.memoredDelta[i] = delta;
	  }
	  
	  if(bias)
		  biasWeight += speedFactor * deltaWeight;
  }
    
  public double calcSum(double... inputs) {
	  if (this.weights.length != inputs.length) {
		  throw new IllegalArgumentException();
	  }
 
	  double sum = 0;
	  for (int i = 0; i < inputs.length; i++) {
		  sum += this.weights[i] * inputs[i];
	  }
	  
	  if(bias)
		  sum += biasWeight;
	  return sum;
  }
  
  public double calcOutput(double...inputs) {
	  double weightedSum = this.calcSum(inputs);
	  return this.activationFunc.getValueOfFunc(weightedSum);
  }
  
  public double getValueOfActivationFunc(double x) {
	  return this.activationFunc.getValueOfFunc(x);
  }
	  
  public double getValueOfDerivativeActivationFunc(double x) {
    return this.activationFunc.getValueOfDerivative(x);
  }
  
  public double getWeight(int index) {
	  return weights[index];
  }
  
  public double[] getWeights() {
	  return this.weights.clone();
  }
  
}
