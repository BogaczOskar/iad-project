package shapes;

import java.awt.geom.Point2D;
import java.util.ArrayList;

public class Line {
  
  public Line(double a, double b, double c) {
    this.A = a;
    this.B = b;
    this.C = c;
  }
  
  public void changeCoefficients(int factor, Point2D.Double point) {
    double newA = this.A + (factor * (0.001) * point.getX());
    double newB = this.B + (factor * (0.001) * point.getY());
    
    double tmp =  Math.sqrt((newA * newA) + (newB * newB));
    
    this.A = newA / tmp;
    this.B = newB / tmp;
  }
  
  public int getInfoAboutPoint(Point2D.Double point) {
    double leftSide = point.getY();
    double rightSide = ((-this.A / this.B) * point.getX() - (this.C / this.B));
    
    if (leftSide > rightSide) return 1;
    if (leftSide < rightSide) return -1;
    return 0;
  }
  
  public ArrayList<Point2D.Double> getLinePoints(double x1, double x2) {
    double y1 = (-this.A / this.B) * x1 + (-this.C / this.B);
    double y2 = (-this.A / this.B) * x2 + (-this.C / this.B);
    ArrayList<Point2D.Double> p = new ArrayList<>();
    p.add(new Point2D.Double(x1, y1));
    p.add(new Point2D.Double(x2, y2));
    return p;
  }
  
  public double calcDistanceFromPointToLine(Point2D.Double point) {
    return (Math.abs(this.A * point.getX() + this.B * point.getY() + this.C)) / 
        Math.sqrt((this.A * this.A) + (this.B * this.B));
  }
  
  public ArrayList<ArrayList<Point2D.Double>> separatePoints(ArrayList<Point2D.Double> points, double range) {
    ArrayList<Point2D.Double> near = new ArrayList<>();
    ArrayList<Point2D.Double> far = new ArrayList<>();
    
    for (Point2D.Double point : points) {
      if (this.calcDistanceFromPointToLine(point) > range) {
        far.add(point);
      } else {
        near.add(point);
      }
    }
    
    ArrayList<ArrayList<Point2D.Double>> result = new ArrayList<>();
    result.add(near);
    result.add(far);
    
    return result;
  }
  
  public double getA() {
    return A;
  }
  
  public void setA(double a) {
    this.A = a;
  }
  
  public double getB() {
    return this.B;
  }
  
  public void setB(double b) {
    this.B = b;
  }
  
  public double getC() {
    return this.C;
  }
  
  public void setC(double c) {
    this.C = c;
  }
  
  public void setFactors(double a, double b, double c) {
    this.setA(a);
    this.setB(b);
    this.setC(c);
  }
  
  private double A;
  private double B;
  private double C;
}
